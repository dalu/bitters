/*
 * Copyright (c) 2023
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __BITTERS__GPIO__H
#define __BITTERS__GPIO__H

/**
 * @file  gpio.c
 * @brief GPIO interface
 *
 * @addtogroup Bitters
 * @{
 */


#include <stddef.h>
#include <stdint.h>
#include <poll.h>


/*== Constants =========================================================*/

/**
 * Input direction for GPIO pin
 */
#define BITTERS_GPIO_DIR_INPUT 				0
/**
 * Ouput direction for GPIO pin
 */
#define BITTERS_GPIO_DIR_OUTPUT				1


/**
 * Use behaviour defined by hardware
 * See: - raspio-gpio  (see: raspi-gpio help)
 *      - device-tree with brcm,pull (see: brcm,bcm2835-gpio.txt)
 *      - config.txt (see: config-txt/gpio.md)
 */
#define BITTERS_GPIO_MODE_DEFAULT			0
/**
 * Configure open drain beahviour for GPIO pin
 */
#define BITTERS_GPIO_MODE_OPEN_DRAIN			1
/**
 * Configure open drain source for GPIO pin
 */
#define BITTERS_GPIO_MODE_OPEN_SOURCE			2


/**
 * Use behaviour defined by hardware
 * See: - raspio-gpio  (see: raspi-gpio help)
 *      - device-tree with brcm,pull (see: brcm,bcm2835-gpio.txt)
 *      - config.txt (see: config-txt/gpio.md)
 */
#define BITTERS_GPIO_BIAS_DEFAULT			0
/**
 * Disable bias behaviour.
 */
#define BITTERS_GPIO_BIAS_DISABLED			1
/**
 * Configure pull-up behaviour for GPIO pin
 */
#define BITTERS_GPIO_BIAS_PULL_UP			2
/**
 * Configure pull-down behaviour for GPIO pin
 */
#define BITTERS_GPIO_BIAS_PULL_DOWN			3


/**
 * Disable interrupt processing
 */
#define BITTERS_GPIO_INTERRUPT_DISABLED			0x0
/**
 * Process interrupt on rising edge
 */
#define BITTERS_GPIO_INTERRUPT_RISING_EDGE		0x1
/**
 * Process interrupt on falling edge
 */
#define BITTERS_GPIO_INTERRUPT_FALLING_EDGE		0x2
/**
 * Process interrupt on raising and falling edge
 */
#define BITTERS_GPIO_INTERRUPT_BOTH_EDGE		0x3


/**
 * GPIO pin not used
 */
#define BITTERS_GPIO_PIN_NONE   			NULL


/**
 * Value to use when polling on gpio
 */
#define BITTERS_GPIO_POLL_EVENTS			POLLPRI | POLLIN


/*
 * Mark pin as enabled for interrupt processing
 */
#define GPIO_PIN_FLAG_INTERRUPT		0x01




/*== Macros ============================================================*/

/**
 * Initialize a GPIO pin
 * Ex: bitters_gpio_pin_t rst = BITTERS_GPIO_PIN_INITIALIZER(dev, pin);
 */
#define BITTERS_GPIO_PIN_INITIALIZER(_dev, _pin)			\
    {									\
       .id           = (_pin ),						\
       .ctrl_devname = (_dev),						\
       .ctrl         = NULL,						\
       .fd           = -1,						\
   }


/**
 * Extract the file descriptor use for irq processing from the pin
 * @note Undefined behaviour if used with bitters_gpio_irq_callback
 *
 * @param pin 		pin identification
 * @return -1           if pin not enabled for interrupt processing
 * @return		file descriptor
 *
 * ~~~
 * struct pollfd pfd = { .fd     = BITTERS_GPIO_IRQ_FD(pin),
 *                       .events = BITTERS_GPIO_POLL_EVENTS }
 * ~~~
 */
#define BITTERS_GPIO_IRQ_FD(_pin)					\
    ((pin->flags & GPIO_PIN_FLAG_INTERRUPT) ? (_pin)->fd : -1)




/*== Structures ========================================================*/

/* Forward declaration */
struct bitters_gpio_ctrl;
struct bitters_gpio_pin;

typedef void (*bitters_gpio_irq_cb_t)(struct bitters_gpio_pin *pin, void *args);
    

/**
 * GPIO pin configuration.
 */
typedef struct bitters_gpio_cfg {
    char    *label;	/**< IO: informative label for system information */
    uint8_t  dir;	/**< IO: gpio direction (input | output)  	  */
    uint8_t  mode;	/**<  O: gpio mode (open drain, open source, ...) */
    uint8_t  bias;	/**< I : gpio bias (pull-up, pull-down, ...)	  */
    uint8_t  interrupt;	/**< I : interrupt processing 			  */
    uint32_t debounce;  /**< I :debounce for input, 0 = none              */
    int      defval;	/**<  O: default value when enabling output 	  */
} bitters_gpio_cfg_t;


/**
 * GPIO pin definition.
 */
typedef struct bitters_gpio_pin {
    const int   id;			/**< pin id			*/
    const char const *ctrl_devname;	/**< controller device name	*/
    /* private */
    uint8_t flags;			// Flags for configuration state
    struct bitters_gpio_ctrl *ctrl;	// Back pointer on controller
    int fd;				// File descriptor for pin
#if defined(BITTERS_WITH_THREADS)
    bitters_gpio_irq_cb_t irq_cb;	// Callback for irq processing
    void *irq_cb_args;			// Data pointer for irq callback
#endif
} bitters_gpio_pin_t;


int bitters_gpio_init(void);

/**
 * Enable the pin according to the selected configuration.
 *
 * @param pin 		pin identification
 * @param cfg		pin configuration
 * @return < 0 in case of error (-errno)
 */
int bitters_gpio_pin_enable(bitters_gpio_pin_t *pin, bitters_gpio_cfg_t *cfg);

/**
 * Disable the pin.
 *
 * @param pin 		pin identification
 * @return < 0 in case of error (-errno)
 */

int bitters_gpio_pin_disable(bitters_gpio_pin_t *pin);
/**
 * Read pin value.
 *
 * @param pin 		pin identification
 * @param value		pin value (0=low, 1=high)
 * @return < 0 in case of error (-errno)
 */

int bitters_gpio_pin_read(bitters_gpio_pin_t *pin, int *value);

/**
 * Write value to the pin.
 *
 * @param pin 		pin identification
 * @param value		pin value (0=low, 1=high)
 * @return < 0 in case of error (-errno)
 */
int bitters_gpio_pin_write(bitters_gpio_pin_t *pin, int value);

/**
 * Wait for interrupt on pin.
 * @note Undefined behaviour if used with bitters_gpio_irq_callback
 *
 * @param pin 		pin identification
 * @return -EINVAL	if pin was not enabled for interrupt
 * @return < 0 in case of error (-errno)
 */
int bitters_gpio_irq_wait(bitters_gpio_pin_t *pin);

/**
 * Fill a pollfd structure, allowing to explicitely perform
 * a poll/ppoll request combining several file descriptor
 * @note Undefined behaviour if used with bitters_gpio_irq_callback
 *
 * @param pin 		pin identification
 * @param pfd[out]      pointer to a pollfd structure
 * @return -EINVAL	if pin was not enabled for interrupt
 */
int bitters_gpio_irq_fill_poolfd(bitters_gpio_pin_t *pin, struct pollfd *pfd);

#if defined(BITTERS_WITH_THREADS) || defined(__DOXYGEN__)
/**
 * Register a callback for processing interrupt on pin.
 * @note Undefined behaviour if used with bitters_gpio_irq_wait or poll
 *
 * @param pin 		pin identification
 * @param cb		callback (use NULL to disable)
 * @param args		argument passed to the callback
 * @return -ENOSYS	if not compiled with thread support
 * @return < 0 in case of error (-errno)
 */
int bitters_gpio_irq_callback(bitters_gpio_pin_t *pin,
			      bitters_gpio_irq_cb_t cb, void *args);
#endif
/** @} */

#endif
